using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed;
    public float jumpForce;


    
    public Rigidbody2D rb;

    bool isGrounded;
    float tempMoveSpeed;

    private void Start()
    {
        tempMoveSpeed = moveSpeed;
        isGrounded = false;
    }


    void Update()
    {
        rb.velocity = new Vector2(Input.GetAxisRaw("Horizontal")*tempMoveSpeed, rb.velocity.y);
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            isGrounded = false;
            rb.velocity = Vector2.up * jumpForce;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ground")
        {
            isGrounded = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ground")
        {
            isGrounded = false;
        }
    }
}
